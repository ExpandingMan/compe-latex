#====================================================================================================
This script generates latex symbols and names from the Julia RPL list and outputs a lua file
====================================================================================================#

using REPL

function unicode_list()
    s = [sort!(collect(REPL.REPLCompletions.latex_symbols), by=(x -> x[1]));
         sort!(collect(REPL.REPLCompletions.emoji_symbols), by=(x -> x[1]))]
end

function luaitem(io::IO, p::Pair)
    k, v = p
    k = replace(k, "\\"=>"\\\\")
    kq = "\"$k\""
    vq = "\"$v\""
    label = '"'*k*" "*v*'"'
    write(io, "{word=$kq, label=$label, insertText=$vq, filterText=$kq}")
end

function luaitems(io::IO, l=unicode_list()) 
    foreach(l) do p
        print(io, "  ")
        luaitem(io, p)
        print(io, ",\n")
    end
end

function luafile(io::IO, l=unicode_list(), objname="symbols")
    print(io, "local $objname = {\n")
    luaitems(io, l)
    print(io, "}\n")
    print(io, "\n")
    print(io, "return $objname")
end

function luafile(fname::AbstractString=joinpath(@__DIR__,"lua","cmp_latex","items.lua"),
                 l=unicode_list()
                )
    open(io -> luafile(io, l), fname, write=true, create=true)
end
