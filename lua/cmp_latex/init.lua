local source = {}

local items = require("cmp_latex.items")

function source:new()
  return setmetatable({}, { __index = source })
end

function source:get_debug_name()
    return "latex_symbols"
end

function source:get_trigger_characters()
  return { "\\" }
end

function source:get_keyword_pattern()
  return "\\\\[^[:blank:]]*"
end

function source:complete(params, callback)
  callback(items)
end

require("cmp").register_source("latex_symbols", source.new())

return source
