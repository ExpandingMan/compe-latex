# `cmp-latex`
This is a LaTeX-to-unicode source extension for [`nvim-cmp`](https://github.com/hrsh7th/nvim-cmp).

## Installation
First of course you must install [`nvim-cmp`](https://github.com/hrsh7th/nvim-cmp).
`nvim-cmp` requires an `nvim` version ≥ `0.5`.

Using [`packer.nvim`](https://github.com/wbthomason/packer.nvim) or similar, put
```lua
use 'https://gitlab.com/ExpandingMan/compe-latex'
```
into your `startup`.

The source is known to `cmp` as `latex_symbols`, so you must, at a minimum, add
```lua
{name = "latex_symbols"}
```
to the `sources` argument to `cmp.setup`.

## Usage
This provides matching of LaTeX commands using the `\` character.  For example `\alpha`
will show the suggestion `α`.  The complete list of available commands is taken from the
Julia language REPL and can be found
[here](https://docs.julialang.org/en/v1/manual/unicode-input/).  These include emojis, so
it suggested (but not required) that you do not use this with the `compe` `emoji` source.

## Other Packages
This package deliberately maintains a list identical to what is used in the Julia REPL, which is the
same list used by [`julia-vim`](https://github.com/JuliaEditorSupport/julia-vim).

For a possibly more expansive alternative see [`cmp-latex-symbols`](https://github.com/kdheepak/cmp-latex-symbols).

